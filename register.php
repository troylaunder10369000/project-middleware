<?php

include 'connection.php';
ini_set("display_errors", "off");

$param = (isset($_GET["forename"]) ? $_GET['forename'] : null);
$param2 = (isset($_GET["surname"]) ? $_GET['surname'] : null);
$param3 = (isset($_GET["email"]) ? $_GET['email'] : null);
$param4 = (isset($_GET["password"]) ? $_GET['password'] : null);

//$param = '3,5,6';
$forename = mysql_real_escape_string($param);
$surname = mysql_real_escape_string($param2);
$email = mysql_real_escape_string($param3);
$password = mysql_real_escape_string($param4);

//testing the connection to my database. 
if (!$conn) {
    die("connection failed: " . mysqli_connect_error());
}
//the query i want to run from my database
//SELECT * FROM `quiz` WHERE ID NOT IN (1,2,3)
$sql = $conn->prepare("INSERT INTO `users` (`id`, `forename`, `surname`, `email`, `password`) VALUES (:id, :forename, :surname, :email, :password)");

$sql->bindValue(':id', null);
$sql->bindParam(':forename', $forename);
$sql->bindParam(':surname', $surname);
$sql->bindParam(':email', $email);
$sql->bindParam(':password', $password);

if ($sql->execute() == true)
{

    $arr = array('success' => 'success');
    $result = json_encode(array('register' => $arr));
    
}
else 
{
        //error reply
    $arr = array('success' => 'fail');
    $result = json_encode(array('register' => $arr));
}
print $result;

?>

